import py4j.GatewayServer;
import java.lang.reflect.*;
import java.util.*;

public class Entrypoint {
    public static Object getObj(String pkgName, String className, List args) {
        Object obj = null;
        try {
            Class cls2 = Class.forName(pkgName + '.' + className);
            int num_of_args = args.size();
            Class[] cArg = new Class[num_of_args];
            Object[] cArg_val = new Object[num_of_args];
            /* code to parse args to fill cArg and cArg_val */
            Constructor ctor = cls2.getDeclaredConstructor(cArg);
            obj = ctor.newInstance(cArg_val);
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException x) {
            x.printStackTrace();
        }
        /* other exception catchers */
        return obj; // this is general Object type, hence I need to typecast in python
    }

    public static void main(String[] args) {
        GatewayServer gatewayServer = new GatewayServer(new Entrypoint());
        gatewayServer.start();
        System.out.println("Gateway Server Started");
    }
}
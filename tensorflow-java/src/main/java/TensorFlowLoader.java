import org.tensorflow.Graph;
import org.tensorflow.Session;
import org.tensorflow.Tensor;
import org.tensorflow.TensorFlow;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TensorFlowLoader {

    private static byte[] getGraph() {
        byte[] graph = null;
        try {
            Path modelPath = Paths.get(TensorFlowLoader.class.getResource("saved_model.pb").toURI());
            graph = Files.readAllBytes(modelPath);
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
        return graph;
    }

    private static float[][] predict(Session sess, Tensor inputTensor) {
        Tensor result = sess.runner()
                .feed("input", inputTensor)
                .fetch("not_activated_output")
                .run()
                .get(0);
        float[][] outputBuffer = new float[1][3];
        result.copyTo(outputBuffer);
        return outputBuffer;
    }

    public static void main(String[] args) {
        System.out.println("TensorFlow version: " + TensorFlow.version());

        try (Graph g = new Graph()) {
            g.importGraphDef(getGraph());

            //debug
            System.out.println(g.operation("input").output(0));
            System.out.println(g.operation("not_activated_output").output(0));
            System.out.println(g.operation("output").output(0));


            //open session using imported graph
            try (Session sess = new Session(g)) {
                float[][] inputData = {{4, 3, 2, 1}};

                // We have to create tensor to feed it to session,
                // unlike in Python where you just pass Numpy array
                Tensor inputTensor = Tensor.create(inputData, Float.class);
                float[][] output = predict(sess, inputTensor);
                for (int i = 0; i < output[0].length; i++) {
                    System.out.println(output[0][i]);
                }
            }
        }
    }
}
